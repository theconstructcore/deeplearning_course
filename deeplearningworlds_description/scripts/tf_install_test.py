#!/usr/bin/env python
# This is a python testing TensorFlowInstalation:
# Python
import tensorflow as tf
hello = tf.constant('Hello, TensorFlow!')
sess = tf.Session()
print(sess.run(hello))
