#!/usr/bin/env python
from __future__ import print_function
# This is to pring print funtion of python 3 to python 2.7 to allow separators in print

"""
This is the core tutorial for learning the basics of Tensor Flow
"""
import os
"""
TF_CPP_MIN_LOG_LEVEL is a TensorFlow environment variable responsible
for the logs, to silence INFO logs set it to 1, to filter out WARNING 2
and to additionally silence ERROR logs (not recommended) set it to 3
"""
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

print ("Importing tensor flow...")
import tensorflow as tf
print ("DONE Importing tensor flow...")

node1 = tf.constant(3.0, dtype=tf.float32, name="node1")
node2 = tf.constant(4.0, dtype=tf.float32, name="node2")

# Here we just get info of the nodes, not evaluating anything
print (node1,node2)

# Here we apply apperations
node3 = tf.add(node1, node2, name="add")

print("node3:", node3)

# Init Session
sess = tf.Session()

# We start the TensorBoard System to visualise learning
writer = tf.summary.FileWriter('./graphs', sess.graph)
"""
execute in shell: tensorboard --logdir=./graphs
and connect through the webbroeswer: http://instance_ip:6006
"""

print (sess.run([node1,node2]))
print("sess.run(node3):", sess.run(node3))

writer.close()