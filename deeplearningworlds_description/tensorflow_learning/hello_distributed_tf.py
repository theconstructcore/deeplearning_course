#!/usr/bin/env python
from __future__ import print_function
import os
"""
TF_CPP_MIN_LOG_LEVEL is a TensorFlow environment variable responsible
for the logs, to silence INFO logs set it to 1, to filter out WARNING 2
and to additionally silence ERROR logs (not recommended) set it to 3
"""
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'


print ("Importing tensor flow...")
import tensorflow as tf
print ("DONE Importing tensor flow...")


c = tf.constant("Hello, distributed TensorFlow!")
server = tf.train.Server.create_local_server()
sess = tf.Session(server.target)  # Create a session on the server.

print("sess.run(c):", sess.run(c))